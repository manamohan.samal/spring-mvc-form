<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Add</title>
</head>
<body>
	<center>
		<h2 style="color: blue;">Add User To DB</h2>
		<form:form method="post" action="add" modelAttribute="userDTO">
			<table>
				<tr>
					<td>Name :</td>
					<td><form:input path="name" /></td>
				</tr>
				<tr>
					<td>UserName :</td>
					<td><form:input path="username" /></td>
				</tr>
				<tr>
					<td>Password :</td>
					<td><form:input path="password" /></td>
				</tr>
				<tr>
					<td>Address :</td>
					<td><form:input path="address" /></td>
				</tr>
				<tr>
					<td>Phone :</td>
					<td><form:input path="phone" /></td>
				</tr>
				<tr>
					<td><input type="submit" value="Add USer"></td>
					<td><input type="reset" value="Cancel"></td>
				</tr>
			</table>
		</form:form>
	</center>
</body>
</html>