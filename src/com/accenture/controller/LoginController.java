package com.accenture.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.accenture.model.ResponseModel;
import com.accenture.service.LoginService;

//@Controller
public class LoginController {

//	@Autowired
	private LoginService loginService;
	
	@RequestMapping("/")
	public String index() {
		System.out.println("LoginController.index()");		
		return "login";
	}

	@RequestMapping(value = "/validate", method = RequestMethod.POST)
	public ModelAndView validateLogin(@RequestParam("userName") String userName,
			@RequestParam("password") String password) {
		ModelAndView model = new ModelAndView();

		String message = loginService.validateLogin(userName, password);
		ResponseModel response = ResponseModel.builder().message(message).data(userName).build();
		if ("success".equals(message)) {
			model.setViewName("success");
		} else {
			model.setViewName("failure");
		}

		model.addObject("result", response);
		return model;
	}
}
