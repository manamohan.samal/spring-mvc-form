package com.accenture.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.accenture.dto.UserDTO;
import com.accenture.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService service;

	@RequestMapping("/add")
	public String index(Model model) {
		model.addAttribute("userDTO", new UserDTO());
		return "user/add";
	}
	
	@ResponseBody
	@RequestMapping(value="/add", method= RequestMethod.POST)
	public String addUser(@ModelAttribute UserDTO userDTO) {
		UserDTO dto = new UserDTO();
		
		dto = service.addUser(userDTO);
		System.out.println(dto);
		
		return "Success";
		
	}
}
