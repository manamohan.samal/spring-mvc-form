package com.accenture.service;

import org.springframework.beans.BeanUtils;

import com.accenture.dto.UserDTO;
import com.accenture.entity.UserEntity;

public interface UserService {

	public UserDTO addUser(UserDTO userDto) ;
	
	
	
	public static UserDTO convertUserEntityToUserDTO(UserEntity userEntity) {
		UserDTO dto = new UserDTO();
		BeanUtils.copyProperties(userEntity, dto);
		return dto;
	}
	
	public static UserEntity convertUserDTOToUserEntity(UserDTO userDTO) {
		UserEntity entity = new UserEntity();
		BeanUtils.copyProperties(userDTO, entity);
		return entity;
	}
}
