package com.accenture.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.dao.UserDao;
import com.accenture.dto.UserDTO;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDao repository;

	@Override
	public UserDTO addUser(UserDTO userDto) {
		return UserService.convertUserEntityToUserDTO(
				repository.save(
						UserService.convertUserDTOToUserEntity(userDto)));
	}

}
